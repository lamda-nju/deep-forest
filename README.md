# 深度森林DF21

#### 项目介绍

本项目中的DF21是深度森林的2021.2.1实现版本。深度森林是基于决策树的深度学习模型。使用树模型学习技术（如随机森林、GBDT等）的应用都可以尝试使用DF21。它具有以下的优势：

* 拥有比其他基于决策树的集成学习方法更好的性能
* 拥有更少的超参数，并且无需大量的调参
* 训练效率高，并且能够处理大规模的数据集

#### 安装教程

深度森林的最新稳定版本已经发布在[PyPi](https://pypi.org/)上，您可以通过以下命令安装：

``` bash
$ pip install deep-forest
```

关于Nightly-Build版本以及如何对源代码进行编译请参考官网的[安装说明](http://www.lamda.nju.edu.cn/deep-forest/installation_guide.html)

#### 使用说明

深度森林采用了与[Scikit-Learn](https://scikit-learn.org/stable/)类似的API设计。例如，下面的代码片段展示了在安装完成后，如何将深度森林利用在一个简单的数字分类数据集上：

``` python
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

from deepforest import CascadeForestClassifier

X, y = load_digits(return_X_y=True)
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
model = CascadeForestClassifier(random_state=1)
model.fit(X_train, y_train)
y_pred = model.predict(X_test)
acc = accuracy_score(y_test, y_pred) * 100
print("\nTesting Accuracy: {:.3f} %".format(acc))
>>> Testing Accuracy: 98.667 %
```

关于深度森林在实际应用中的性能，以及与其他基于决策树的集成学习方法的对比，请参考官网的[实验部分](http://www.lamda.nju.edu.cn/deep-forest/experiments.html)。

#### 相关资源

* [官方文档](http://www.lamda.nju.edu.cn/deep-forest/)
* 深度森林：[论文](https://arxiv.org/pdf/1702.08835.pdf) | [报告](https://aistats.org/aistats2019/0-AISTATS2019-slides-zhi-hua_zhou.pdf)

#### 相关信息

本项目目前主要由徐轶轩进行开发和维护。在正式发布之前，它已经在LAMDA内部经过测试和使用。